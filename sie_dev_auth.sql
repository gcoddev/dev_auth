-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 18-02-2022 a las 15:08:54
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sie_dev_auth`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

CREATE TABLE `notas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(150) NOT NULL,
  `descricpion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `notas`
--

INSERT INTO `notas` (`id`, `titulo`, `descricpion`) VALUES
(1, 'Nota 1', 'Tarea 1'),
(2, 'Nota 2', 'Tarea 2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `ci` varchar(10) NOT NULL,
  `expedido` varchar(5) DEFAULT NULL,
  `nombre` varchar(50) NOT NULL,
  `paterno` varchar(50) NOT NULL,
  `materno` varchar(50) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `image` longblob DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(256) DEFAULT NULL,
  `creado_el` datetime NOT NULL,
  `modificado_el` datetime NOT NULL,
  `eliminado_el` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `ci`, `expedido`, `nombre`, `paterno`, `materno`, `fecha_nacimiento`, `image`, `password`, `remember_token`, `creado_el`, `modificado_el`, `eliminado_el`) VALUES
(1, '12345678', 'LP', 'JOSE', 'PATERNO', 'MATERNO', '1995-05-01', NULL, '$2y$10$Mqlz.VUeH3wPsDts5udBp.PKa..emBnQnracwnTHLX/iI2gs6yNge', 'Dobyf1izcANqxoRpRxbZcVLaPELiDgIVGNLw57wMVZC7fMZqPsaIuYXn8sRk', '2022-02-15 15:55:37', '2022-02-15 15:57:04', NULL),
(2, '10077835', 'LP', 'Gary', 'Apaza', 'Mamani', '2001-12-01', 0x696d616765732f696d672f313634343934313330332d64657363617267612e6a706567, '$2y$10$4/p1XChvuZSKlnYK.5Bn6edXfA8T.0Izpm1hBREHihT6gOsEyeFh.', 'BkMSf9LsFHskhX0SrOj8go1Wg5kut7Qajl6lMNzEIEYrwx09MZ2O11RKmJoX', '2022-02-15 16:01:13', '2022-02-18 13:41:22', NULL),
(3, '10000001', 'LP', 'Benito', 'Juarez', 'Juarez', '2007-07-07', NULL, '$2y$10$3WBr/.8RJNOD6R8uP4Syt.PC.DdBiBHrA8269MskG3/jXjgRhKcMa', 'pLN2KX3Bcebhzt98rPeuHfold3H58LKqzQZi9ell6CduxZu3mhS8ssYGGYLf', '2022-02-15 16:12:00', '2022-02-15 16:14:47', '2022-02-15 16:14:47'),
(4, '10001000', 'LP', 'Norma', 'Avelino', 'Perez', '2007-07-07', 0x696d616765732f696d672f313634353031363634352d666f6e646f2e706e67, '$2y$10$CAxGXhwtvQ/SVppZOi7AJeoW1cryzRoB3JkwashbBAAzqMytsgZWi', 'b5TPCAaENYASKAdQEfHSiVhm7lgsmx1WyBKCtBcSkv1Zd9L3YECeZHoWi0KJ', '2022-02-16 13:02:30', '2022-02-16 13:10:28', '2022-02-16 13:10:28'),
(5, '10101010', 'LP', 'Jacqueline', 'Pillco', 'Quispe', '2004-02-06', 0x696d616765732f696d672f313634353139333033302d6b61776169692d67617469746f732e676966, '$2y$10$96lglaEYRvUc4XTYWzcfOeZPSdP8FUTFji/HZOqA/r5kRWjARytk2', NULL, '2022-02-18 14:03:14', '2022-02-18 14:03:50', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `notas`
--
ALTER TABLE `notas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
