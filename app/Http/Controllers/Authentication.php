<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class Authentication extends Controller
{
    public function login(Request $request){
        $ci = $request->input('ci');
        $password = $request->input('password');

        /*$usuario = User::where('ci', '=', $ci)->first();
        if(!$usuario){
            return back()->withErrors(['El usuario no existe']);
        }*/


        if(!Auth::attempt(['ci' => $ci, 'password' => $password])) {
            return back()->withErrors(['Credenciales incorrectas']);
        }

        return redirect()->route('pagina_inicio_usuario');
    }


    public function logout(){
        Auth::logout();
        return redirect()->route('pagina_inicio_usuario');
    }
}
