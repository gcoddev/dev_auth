<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\User;

class InicioControlador extends Controller
{
    public function inicio(){
        /*$usuario = new User;
        $usuario->ci = "12345678";
        $usuario->expedido = "LP";
        $usuario->nombre = "ADMIN";
        $usuario->paterno = "ADMIN";
        $usuario->materno = "ADMIN";
        $usuario->fecha_nacimiento = "2000-01-01";
        $usuario->password = Hash::make("admin123");
        $usuario->save();*/

        return view('web.login');
        // $file = file('images/usuario.png');
        // $filename = $file->getClientOriginalName();
        // return $file;
    }

    public function registro() {
        return view('web.registro');
    }
    public function post_registro(Request $request) {
        $validateData = $request->validate([
            'ci' => 'required',
            'expedido' => 'required',
            'nombres' => 'required',
            'paterno' => 'required',
            'materno' => 'required',
            'fecha' => 'required',
            'password1' => 'required',
            'password2' => 'required|same:password1'
        ], [
            'ci.required' => 'CI obligatoria',
            'expedido.required' => 'Expedido obligatorio',
            'nombres.required' => 'Nombres obligatorio',
            'paterno.required' => 'Apellido paterno obligatorio',
            'materno.required' => 'Apeliido materno obligatorio',
            'fecha.required' => 'Fecha de nacimiento obligatorio',
            'password1.required' => 'Contraseña invalida',
            'password2.required' => 'Confirme la contraseña',
            'password2.same' => 'Fallo al confirmar contraseña'
        ]);
        $registro = new User;
        $registro->ci = $request->ci;
        $registro->expedido = $request->expedido;
        $registro->nombre = $request->nombres;
        $registro->paterno = $request->paterno;
        $registro->materno = $request->materno;
        $registro->fecha_nacimiento = $request->fecha;

        // $file = asset('images/usuario.png');
        // $destino = 'images/img/';
        // $filename = time() . '-' . $file->getClientOriginalName();
        // $subir = $request->file('image')->move($destino, $filename);
        // $editar->image = $destino . $filename;

        $registro->password = Hash::make($request->password1);
        $registro->save();
        return redirect()->route('pagina_inicial')->with('mensaje', 'Usuario creado correctamente, ya puede iniciar sesion');
    }
}
