<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;

class Usuario extends Controller
{
    public function inicio(){
        return view('usr.inicio');
    }

    public function pagina_cambiar_password(){
        return view('usr.usuario.cambiar_password');
    }
    
    public function upp(Request $request) {
        $validateData = $request->validate([
            'actual_pass' => 'required'
        ], [
            'actual_pass.required' => 'Ingrese su contraseña, no sea imbecil'
        ]);

        if (!Hash::check($request->actual_pass, Auth::user()->password)) {
            return back()->withErrors(['Contraseña actual incorrecta']);
        } else {
            // $validateData = $request->validate([
            //     'nuevo_pass' => 'required',
            //     'conf_nuevo_pass' => 'required'
            // ], [
            //     'nuevo_pass.required' => 'Ingrese la contraseña nueva contraseña',
            //     'conf_nuevo_pass.required' => 'Confirme su contraseña nueva'
            // ]);
            if (!isset($request->nuevo_pass)) {
                return back()->withErrors(['Error, debe ingresar su contraseña nueva']);
            } else {
                if (!isset($request->conf_nuevo_pass)) {
                    return back()->withErrors(['Error, debe confirmar su contraseña']);
                } else {
                    if ($request->nuevo_pass == $request->conf_nuevo_pass) {
                        $up_pass = User::find(Auth::user()->id_usuario);
                        $up_pass->password = Hash::make($request->nuevo_pass);
                        $up_pass->save();
                        Auth::logout();
                        return redirect()->route('pagina_inicial')->with('mensaje', 'Contraseña actualizada correctamente, inicie nuevamente sesion');
                    } else {
                        // return 'NO';
                        return back()->withErrors(['Error al confirmar contraseña nueva']);
                    }
                }
            }
            // return $request->nuevo_pass.' - '.$request->conf_nuevo_pass;
            // $pass1 = Hash::make($request->nuevo_pass);
            // $pass2 = Hash::make($request->conf_nuevo_pass);
            // return $pass1.' - '.$pass2;
            // if (!Hash::check($pass2, $pass1)) {
        }
    }

    //Hash::check('passwordToCheck', Auth::user()->password)

    public function perfil() {
        return view('usr.usuario.perfil');
    }

    public function imagen(Request $request) {
        $validateData = $request->validate([
            'image' => 'required'
        ], [
            'image.required' => 'No a ingresado ninguna imagen'
        ]);
        if (isset(Auth::user()->image)) {
            unlink(Auth::user()->image);
        }
        $editar = User::findOrFail(Auth::user()->id_usuario);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $destino = 'images/img/';
            $filename = time() . '-' . $file->getClientOriginalName();
            $subir = $request->file('image')->move($destino, $filename);
            $editar->image = $destino . $filename;
        }
        $editar->save();
        return redirect()->route('perfil')->with('mensaje', 'Imagen agregaga correctamente');
    }

    public function pagina_editar_user() {
        return view('usr.usuario.editar_usuario');
    }

    public function upu(Request $request) {
        $validateData = $request->validate([
            'nombres' => 'required',
            'paterno' => 'required',
            'materno' => 'required'
        ], [
            'nombres.required' => 'Campo nombre obligatorio',
            'paterno.required' => 'Campo apellido paterno obligatorio',
            'materno.required' => 'Campo apellido materno obligatorio'
        ]);
        $user = User::findOrFail(Auth::user()->id_usuario);
        $user->nombre = $request->nombres;
        $user->paterno = $request->paterno;
        $user->materno = $request->materno;
        $user->save();
        return redirect()->route('perfil')->with('mensaje', 'Usuario actualizado correctamente');
    }

    public function eliminar(Request $request) {
        $password = $request->password;
        if (isset($password)) {
            if(!Hash::check($password, Auth::user()->password)) {
                return back()->withErrors(['Contraseña incorrecta']);
            }
        } else {
            return back()->withErrors(['Ingrese su contraseña']);
        }
        $id = Auth::user()->id_usuario;

        // Elimina la imagen
        if (isset(Auth::user()->image)) {
            unlink(Auth::user()->image);
        }
        // Cierra sesion
        Auth::logout();

        $delete = User::findOrFail($id);
        $delete->delete();
        return redirect()->route('pagina_inicial')->with('mensaje', 'Usuario eliminado correctamente');
    }
}
