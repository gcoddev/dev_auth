<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'InicioControlador@inicio')->middleware(['usuario_no_autenticado'])->name('pagina_inicial');

Route::post('login', 'Authentication@login')->middleware(['usuario_no_autenticado'])->name('post_login');

Route::get('/registro', 'InicioControlador@registro')->middleware(['usuario_no_autenticado'])->name('registro');

Route::post('/registro', 'InicioControlador@post_registro')->middleware(['usuario_no_autenticado'])->name('post_registro');

Route::prefix('usuario')->middleware(['usuario_autenticado'])->group(function(){
    Route::post('logout', 'Authentication@logout')->name('post_logout');
    Route::get('/', 'Usuario@inicio')->name('pagina_inicio_usuario');

    Route::get('cambiar-password', 'Usuario@pagina_cambiar_password')->name('pagina_cambiar_password');

    Route::post('cambiar-password', 'Usuario@upp')->name('upp');

    Route::get('perfil', 'Usuario@perfil')->name('perfil');

    Route::post('perfil', 'Usuario@imagen')->name('imagen');

    Route::delete('perfil', 'Usuario@eliminar')->name('eliminar');

    Route::get('editar-user', 'Usuario@pagina_editar_user')->name('pagina_editar_usuario');

    Route::post('editar-user', 'Usuario@upu')->name('upu');
});