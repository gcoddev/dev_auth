<html>
    <head>
        <title>{{ isset($titulo_pagina) ? $titulo_pagina : env('APP_NAME') }}</title>
        <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css')  }}">
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <div class="container mt-3">
            @yield('contenido')
        </div>
        
        <script src="{{ URL::asset('js/jquery-3.6.0.min.js')  }}"></script>
        <script src="{{ URL::asset('js/bootstrap.min.js')  }}"></script>
    </body>
</html>