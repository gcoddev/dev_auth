@extends('web.body')

@section('contenido')
    <h1>Registro de nuevo usuario</h1>
    <form action="" method="post" class="form-control">
        {{ csrf_field() }}
        <label for="ci" class="form-check-label">CI:</label>
        <input type="text" name="ci" id="ci" placeholder="Ingrese su numero de Carnet" class="form-control" value="{{ old('ci') }}">

        <label for="expedido" class="form-check-label">Expedido:</label>
        <select name="expedido" id="expedido" class="form-control">
            <option value="">-- Seleccione una opcion</option>
            <option value="LP" {{ old('expedido') === 'LP' ? 'selected' : '' }}>LP</option>
        </select>
        
        <label for="nombres" class="form-check-label">Nombres:</label>
        <input type="text" name="nombres" id="nombres" placeholder="Ingrese su nombre" class="form-control" value="{{ old('nombres') }}">

        <label for="paterno" class="form-check-label">Apellido paterno:</label>
        <input type="text" name="paterno" id="paterno" placeholder="Ingrese su apellido paterno" class="form-control" value="{{ old('paterno') }}">

        <label for="materno" class="form-check-label">Apellido materno:</label>
        <input type="text" name="materno" id="materno" placeholder="Ingrese su apellido materno" class="form-control" value="{{ old('materno') }}">

        <label for="fecha" class="form-check-label">Fecha de nacimiento:</label>
        <input type="date" name="fecha" id="fecha" placeholder="Ingrese su fecha de nacimiento" class="form-control" value="{{ old('fecha') }}">

        <br>

        <label for="password1" class="form-check-label">Contraseña:</label>
        <input type="password" name="password1" id="password1" placeholder="Ingrese su contraseña" class="form-control">

        <label for="password2" class="form-check-label">Confirme su contraseña:</label>
        <input type="password" name="password2" id="password2" placeholder="Confirme su contraseña" class="form-control">

        <input type="submit" value="registrar" class="btn btn-success mt-2">
    </form>
    <br>
    @include('errores')
@endsection