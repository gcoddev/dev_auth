@extends('web.body')

@section('contenido')
    @if (session('mensaje'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('mensaje') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <a href="{{ route('registro') }}" class="mt-3">Registro de usuario</a>
    <form action="{{ route('post_login') }}" method="POST" class="form-control my-3 w-50 m-auto">
        {{ csrf_field() }}
        <label for="ci">Número de CI</label>
        <input type="text" class="form-control" name="ci" id="ci" value="{{ old('ci') }}">
        <label for="exampleInputPassword1">Contraseña</label>
        <input type="password" class="form-control" name="password">
        <button type="submit" class="btn btn-primary mt-2">Ingresar</button>
    </form>
    @include('errores')
@endsection