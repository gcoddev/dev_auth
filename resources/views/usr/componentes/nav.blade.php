{{-- <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="{{ route('pagina_inicio_usuario') }}">{{ env('APP_NAME') }}</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {{ Auth::user()->nombre }}
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          <a class="dropdown-item disabled" href="#">Action</a>
          <a class="dropdown-item disabled" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" onclick="$('#form_logout').submit()">Salir</a>
          <form action="{{ route('post_logout') }}" method="POST" style="display: none;" id="form_logout">
              {{ csrf_field() }}
          </form>
        </div>
      </li>
    </ul>
  </div>
</nav> --}}


<div class="main-header">
    <div class="logo">
        <img src="{{ asset('images/logo.png') }}" alt="">
    </div>
    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>
    
    <div style="margin: auto"></div>
    <div class="header-part-right">
        <!-- Full screen toggle -->
        <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen=""></i>
    
        <!-- User avatar dropdown -->
        <div class="dropdown">
            <div class="user col align-self-end">
              @if (isset(Auth::user()->image))
                <img src="{{ asset(Auth::user()->image) }}" title="{{ Auth::user()->id_usuario }}" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="object-fit: cover">
              @else
                <img src="{{ asset('images/usuario.png') }}" title="{{ Auth::user()->id_usuario }}" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              @endif
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <div class="dropdown-header">
                        <i class="i-Lock-User mr-1"></i> {{ Auth::user()->nombre }} {{ Auth::user()->paterno }} {{ Auth::user()->materno }}
                    </div>
                    <a class="dropdown-item" href="{{ route('perfil') }}">Perfil usuario</a>
                    <a class="dropdown-item" href="{{ route('pagina_cambiar_password') }}">Cambiar contraseña</a>
                    <a class="dropdown-item" onclick="$('#form_logout').submit()" style="cursor: pointer;">Salir</a>
                    <form action="{{ route('post_logout') }}" method="POST" style="display: none;" id="form_logout">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>