@extends('usr.body')
@section('contenido')
    <h1>Editar contraseña</h1>
    <form action="{{ route('upp') }}" method="POST">
        {{ csrf_field() }}
        <label for="" class="form-check-label">Contraseña actual</label>
        <input type="password" class="form-control" name="actual_pass">
        <br>
        <label for="" class="form-check-label">Contraseña nueva</label>
        <input type="password" class="form-control" name="nuevo_pass">
        <label for="" class="form-check-label">Confirme contraseña nueva</label>
        <input type="password" class="form-control" name="conf_nuevo_pass">

        <input type="submit" value="Cambiar contraseña" class="btn btn-primary mt-2">
    </form>
    <br>
    {{-- @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
                {{ $error }}
            </div>
        @endforeach
    @endif --}}
    @include('errores')
@endsection