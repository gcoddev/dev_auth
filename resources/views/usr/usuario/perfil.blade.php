@extends('usr.body')

@section('contenido')
    <div class="row">
        <div class="col-9">
            <h1>Perfil de usuario</h1>
        </div>
        <div class="col-3">
            <a href="{{ route('pagina_editar_usuario') }}" class="btn btn-outline-primary">Editar usuario</a>
        </div>
    </div>
    @if (session('mensaje'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('mensaje') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="container-fluid">
        <div class="row">
            <div class="col-5">
                <b>CI</b>
            </div>
            <div class="col-7">
                {{ Auth::user()->ci }}
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-5">
                <b>Nombre completo de usuario</b>
            </div>
            <div class="col-7">
                {{ Auth::user()->nombre }}
                {{ Auth::user()->paterno }}
                {{ Auth::user()->materno }}
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-5">
                <b>Fecha de nacimiento</b>
            </div>
            <div class="col-7">
                {{ Auth::user()->fecha_nacimiento }}
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-5">
                <b>Fecha de registro</b>
            </div>
            <div class="col-7">
                {{ Auth::user()->creado_el }}
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-5">
                <b>Fecha de ultima modificacion</b>
            </div>
            <div class="col-7">
                {{ Auth::user()->modificado_el }}
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-5">
                <b>Foto de perfil</b>
            </div>
            <div class="col-7">
                @if (!isset(Auth::user()->image))
                    <img src="{{ asset('images/usuario.png') }}" width="150px" height="150px" style="object-fit: cover">
                @else
                    <img src="{{ asset(Auth::user()->image) }}" height="150px">
                @endif
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-5">
                <b>Cambiar imagen</b>
            </div>
            <div class="col-7">
                <form action="{{ route('imagen') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="file" name="image">
                    <input type="submit" value="Subir" class="btn btn-outline-info d-block mt-2">
                </form>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-5">
                <b class="text-danger">Zona peligrosa</b>
            </div>
            <div class="col-7">
                <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#exampleModal">Eliminar usuario</button>
            </div>
        </div>
        <br>


        {{-- modal --}}
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirmar la eliminacion del usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('eliminar') }}" method="post" id="submit_d">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <label for="recipient-name" class="col-form-label">Contraseña actual:</label>
                        <input type="password" class="form-control" id="recipient-name" placeholder="Ingrese su contraseña" name="password">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-outline-danger" onclick="$('#submit_d').submit()">Confirmar</button>
                </div>
                </div>
            </div>
        </div>
        {{-- end modal --}}
        <br>
        @include('errores')
    </div>
@endsection