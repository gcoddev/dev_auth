@extends('usr.body')

@section('contenido')
    <h1>Editar usuario</h1>
    <form action="{{ route('upu') }}" method="post" class="form-control h-auto">
        {{ csrf_field() }}
        <label for="nombres" class="form-check-label">Nombres:</label>
        <input type="text" id="nombres" name="nombres" placeholder="Ingrese su nombre" class="form-control" value="{{ Auth::user()->nombre }}">

        <label for="nombres" class="form-check-label">Apellido paterno:</label>
        <input type="text" id="paterno" name="paterno" placeholder="Ingrese su  apellido paterno" class="form-control" value="{{ Auth::user()->paterno }}">

        <label for="nombres" class="form-check-label">Apellido materno:</label>
        <input type="text" id="materno" name="materno" placeholder="Ingrese su apellido materno" class="form-control" value="{{ Auth::user()->materno }}">

        {{-- <label for="fecha">Fecha de nacimiento</label>
        <input type="date" class="form-control"> --}}

        <input type="submit" value="Editar usuario" class="mt-2 btn btn-warning">
        <a href="{{ route('pagina_inicio_usuario') }}" class="mt-2 btn btn-dark">Volver</a>
    </form>
    <br>
    @include('errores')
@endsection