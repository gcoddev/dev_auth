<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>{{ isset($titulo_pagina) ? $titulo_pagina : env('APP_NAME') }}</title>
        <link href="{{ URL::asset('css/themes/lite-purple.min.css')  }}" rel="stylesheet" />
        <link href="{{ URL::asset('css/plugins/perfect-scrollbar.min.css')  }}" rel="stylesheet" />
    </head>
    <body class="text-left">
        
        <div class="app-admin-wrap layout-sidebar-compact sidebar-dark-purple sidenav-open clearfix">
            @include('usr.componentes.barra_lateral')
            <div class="main-content-wrap d-flex flex-column">
                @include('usr.componentes.nav')
                <div class="main-content">
                    @yield('contenido')
                </div>
            </div>
        </div>
        

        <script src="{{ URL::asset('js/plugins/jquery-3.3.1.min.js')  }}"></script>
        <script src="{{ URL::asset('js/plugins/bootstrap.bundle.min.js')  }}"></script>
        <script src="{{ URL::asset('js/plugins/perfect-scrollbar.min.js')  }}"></script>
        <script src="{{ URL::asset('js/scripts/script.min.js')  }}"></script>
        <script src="{{ URL::asset('js/scripts/sidebar.compact.script.min.js')  }}"></script>
        <script src="{{ URL::asset('js/scripts/customizer.script.min.js')  }}"></script>
        <script src="{{ URL::asset('js/plugins/echarts.min.js')  }}"></script>
        <script src="{{ URL::asset('js/scripts/echart.options.min.js')  }}"></script>
        <script src="{{ URL::asset('js/scripts/dashboard.v1.script.min.js')  }}"></script>
    </body>
</html>